import os

from .base import *


DEBUG = os.getenv('DEBUG', True)

INSTALLED_APPS += [
	'behave_django',
]

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.sqlite3',
		'NAME': root('apps/todos.sqlite3')	
	}
}
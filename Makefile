TOOL = @python manage.py

run:
	$(TOOL) runserver

migrate:
	$(TOOL) makemigrations
	$(TOOL) migrate

test:
	@coverage run manage.py test
	@coverage report

lint:
	@flake8 apps/

e2e:
	$(TOOL) behave